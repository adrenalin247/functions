const getSum = (str1, str2) => {
  if (!isValid(str1) || !isValid(str2)) {
    return false;
  }

  const convertedStr1 = convertToNumber(str1);
  const convertedStr2 = convertToNumber(str2);

  return (convertedStr1 + convertedStr2).toString();
};

function convertToNumber (str) {
  if (str === '') {
    return 0;
  }

  return parseInt(str);
}

function isValid (str) {
  if (typeof str === 'string') {
    return /^\d*$/.test(str);
  }

  return false;
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postCount = 0;
  let commentsCount = 0;

  listOfPosts.forEach((post) => {
    if (post.author === authorName) {
      postCount++;
    }

    if (post.comments) {
      post.comments.forEach((comment) => {
        if (comment.author === authorName) {
          commentsCount++;
        }
      });
    }
  });

  return `Post:${postCount},comments:${commentsCount}`;
};

const tickets = (people) => {
  const TICKET_COST = 25;
  let billsInCashBox = [];
  let result = null;

  for (const billOfClient1 of people) {
    let tmpBillsIncashBox = billsInCashBox.slice();
    const billOfClient = parseInt(billOfClient1);
    let changeOfClientBill = billOfClient - TICKET_COST;

    tmpBillsIncashBox = tmpBillsIncashBox.map((bill) => {
      if (changeOfClientBill - bill >= 0) {
        changeOfClientBill = changeOfClientBill - bill;
        return 'marked to remove';
      }
      return bill;
    });

    if (changeOfClientBill === 0) {
      billsInCashBox = tmpBillsIncashBox.filter(
        (bill) => bill !== 'marked to remove'
      );
      billsInCashBox.push(billOfClient);
      result = 'YES';
    } else {
      result = 'NO';
      break;
    }
  }

  return result;
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
